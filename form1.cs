using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DVadherAssignment3
{
    public partial class Form1 : Form
    {
        //Initialized the images with image name x and o_300.
        Image X = Properties.Resources.x;
        Image O = Properties.Resources.o_300;

        //XorO isplayer x or O.
        bool XorO = true;
        int turncount = 0;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            foreach (Control p in Controls)
            {
                if (p is PictureBox)
                {
                    p.Click += new System.EventHandler(picBox_Click);
                }
            }

        }

        /// <summary>
        /// Created click event for all pictureboxes to load images. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void picBox_Click(object sender, EventArgs e)
        {
            PictureBox picBox = (PictureBox)sender;
            if (XorO == true)
            {
                picBox.Image = X;
                getWinner();

            }
            else
            {
                picBox.Image = O;
                getWinner();
            }
            XorO = !XorO;
            turncount++;
            picBox.Enabled = false;

            if (turncount == 9)
            {
                MessageBox.Show("Game Draw");
                turncount++;

                NewGame();
            }
        }


        /// <summary>
        /// getWinner is a method which is used to check the winner with 
        /// all the conditions mentioned below.
        /// </summary>
        public bool winGame = false;
        public void getWinner()
        {

            if ((picBox1.Image == X && picBox2.Image == X && picBox3.Image == X))
            {
                winGame = true;

            }
            if ((picBox4.Image == X && picBox5.Image == X && picBox6.Image == X))
            {
                winGame = true;
            }
            if ((picBox7.Image == X && picBox8.Image == X && picBox9.Image == X))
            {
                winGame = true;

            }
            if ((picBox1.Image == X && picBox4.Image == X && picBox7.Image == X))
            {
                winGame = true;

            }
            if ((picBox2.Image == X && picBox5.Image == X && picBox8.Image == X))
            {
                winGame = true;

            }
            if ((picBox3.Image == X && picBox6.Image == X && picBox9.Image == X))
            {
                winGame = true;

            }
            if ((picBox1.Image == X && picBox5.Image == X && picBox9.Image == X))
            {
                winGame = true;
            }
            if ((picBox3.Image == X && picBox5.Image == X && picBox7.Image == X))
            {
                winGame = true;
            }
            if ((picBox1.Image == O && picBox2.Image == O && picBox3.Image == O))
            {
                winGame = true;

            }
            if ((picBox4.Image == O && picBox5.Image == O && picBox6.Image == O))
            {
                winGame = true;


            }
            if ((picBox7.Image == O && picBox8.Image == O && picBox9.Image == O))
            {
                winGame = true;

            }
            if ((picBox1.Image == O && picBox4.Image == O && picBox7.Image == O))
            {
                winGame = true;



            }
            if ((picBox2.Image == O && picBox5.Image == O && picBox8.Image == O))
            {
                winGame = true;


            }
            if ((picBox3.Image == O && picBox6.Image == O && picBox9.Image == O))
            {
                winGame = true;


            }
            if ((picBox1.Image == O && picBox5.Image == O && picBox9.Image == O))
            {
                winGame = true;


            }
            if ((picBox3.Image == O && picBox5.Image == O && picBox7.Image == O))
            {
                winGame = true;


            }

            if (winGame)
            {

                string w = "";
                if (XorO == true)
                    w = "X";
                else w = "O";
                MessageBox.Show("The Winner is" + w + "!  Click ok to restart a new game");
                turncount++;
                XorO = !XorO;
                winGame = false;

                NewGame();




            }

        }

        /// <summary>
        /// ResetGame is a method used to start the new game after every win.
        /// </summary>
        public void NewGame()
        {


            picBox1.Image = null;
            picBox2.Image = null;
            picBox3.Image = null;
            picBox4.Image = null;
            picBox5.Image = null;
            picBox6.Image = null;
            picBox7.Image = null;
            picBox8.Image = null;
            picBox9.Image = null;


            picBox1.Enabled = true;
            picBox2.Enabled = true;
            picBox3.Enabled = true;
            picBox4.Enabled = true;
            picBox5.Enabled = true;
            picBox6.Enabled = true;
            picBox7.Enabled = true;
            picBox8.Enabled = true;
            picBox9.Enabled = true;


            turncount = 0;
            XorO = true;


        }



    }

}
